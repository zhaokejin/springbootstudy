package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo8RedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo8RedisApplication.class, args);
	}
}
