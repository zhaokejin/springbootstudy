package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 筱进GG
 */
@SpringBootApplication
public class SpringbootstudyDemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo1Application.class, args);
	}
}
