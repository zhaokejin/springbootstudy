package com.xiaojingg;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo7JdbctemplateMoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo7JdbctemplateMoreApplication.class, args);
	}

}
