package com.cicoding.integration.user;

import java.util.List;

public interface UserService {

     List<User> list();
}
