package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 筱进GG
 */
@SpringBootApplication
public class SpringbootstudyDemo7SpringDataJpaMoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo7SpringDataJpaMoreApplication.class, args);
	}
}
