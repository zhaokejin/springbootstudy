package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo9MybatisAnnotationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo9MybatisAnnotationApplication.class, args);
	}
}
