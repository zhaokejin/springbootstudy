package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo4VelocityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo4VelocityApplication.class, args);
	}
}
