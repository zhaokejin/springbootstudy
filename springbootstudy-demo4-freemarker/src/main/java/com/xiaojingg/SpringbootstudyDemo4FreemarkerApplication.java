package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo4FreemarkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo4FreemarkerApplication.class, args);
	}
}
