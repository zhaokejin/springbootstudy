package com.xiaojingg.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * 筱进GG
 */
public interface UserRepository extends MongoRepository<User, Long> {

    User findByUsername(String username);

}
