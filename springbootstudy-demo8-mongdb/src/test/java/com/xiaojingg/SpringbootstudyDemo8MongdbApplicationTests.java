package com.xiaojingg;

import com.xiaojingg.domain.User;
import com.xiaojingg.domain.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootstudyDemo8MongdbApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Before
	public void setUp() {
		userRepository.deleteAll();
	}

	@Test
	public void test() throws Exception {

		// 创建三个User，并验证User总数
		userRepository.save(new User(1L, "keke", 30));
		userRepository.save(new User(2L, "GG", 40));
		userRepository.save(new User(3L, "QQ", 50));
		System.out.print("三个User:" + userRepository.findAll().size() + "\n");

		// 删除一个User，再验证User总数
		User u = userRepository.findOne(1L);
		userRepository.delete(u);
		System.out.print("删除一个User:" + userRepository.findAll().size() + "\n");

		// 删除一个User，再验证User总数
		u = userRepository.findByUsername("GG");
		userRepository.delete(u);
		System.out.print("删除一个User:" + userRepository.findAll().size() + "\n");

	}
}
