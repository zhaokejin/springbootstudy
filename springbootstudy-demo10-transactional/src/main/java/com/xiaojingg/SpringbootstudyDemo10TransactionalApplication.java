package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo10TransactionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo10TransactionalApplication.class, args);
	}
}
