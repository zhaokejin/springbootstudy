package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo7SpringDataJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo7SpringDataJpaApplication.class, args);
	}
}
