package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo2Application.class, args);
	}
}
