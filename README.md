# springbootstudy
springbootstudy学习  **我的博客地址** : [http://blog.csdn.net/zhaokejin521](http://blog.csdn.net/zhaokejin521)

[Spring Boot学习(一)初始项目,加入web模块，编写简单的RESTful](http://blog.csdn.net/zhaokejin521/article/details/78440180)

[Spring Boot学习(二)之属性配置文件详解](http://blog.csdn.net/zhaokejin521/article/details/78496500)

[Spring Boot学习(三)之构建RESTful API与单元测试](http://blog.csdn.net/zhaokejin521/article/details/78496697)

[Spring Boot学习(四)之web开发渲染页面 -- Thymeleaf](http://blog.csdn.net/zhaokejin521/article/details/78496748)

[Spring Boot学习(四)之web开发渲染页面 -- Freemarker](http://blog.csdn.net/zhaokejin521/article/details/78496759)

[Spring Boot学习(四)之web开发渲染页面 -- Velocity](http://blog.csdn.net/zhaokejin521/article/details/78496768)

[Spring Boot学习(五)之使用Swagger2构建强大的RESTful API文档](http://blog.csdn.net/zhaokejin521/article/details/78496815)

[Spring Boot学习(六)之Web应用的统一异常处理](http://blog.csdn.net/zhaokejin521/article/details/78568635)

[Spring Boot学习(七)之Web应用使用JdbcTemplate访问数据库](http://blog.csdn.net/zhaokejin521/article/details/78568668)

[Spring Boot学习(七)之Web应用使用Spring-data-jpa让数据访问](http://blog.csdn.net/zhaokejin521/article/details/78568673)

[Spring Boot学习(七)之Web应用使用jdbctemplate多数据源配置](http://blog.csdn.net/zhaokejin521/article/details/78595337)

[Spring Boot学习(七)之Web应用使用Spring-data-jpa多数据源配置](http://blog.csdn.net/zhaokejin521/article/details/78624258)

[Spring Boot学习(八)之使用NoSQL数据库（一）：Redis](http://blog.csdn.net/zhaokejin521/article/details/78656095)

[Spring Boot学习(八)之使用NoSQL数据库（二）：MongDb](http://blog.csdn.net/zhaokejin521/article/details/78659149)

[Spring Boot学习(九)之Spring Boot整合MyBatis](http://blog.csdn.net/zhaokejin521/article/details/78923395)

[Spring Boot学习(九)之Spring Boot中使用MyBatis注解配置详解](http://blog.csdn.net/zhaokejin521/article/details/78960460)

[Spring Boot学习(十)之Spring Boot开启声明式事务](http://blog.csdn.net/zhaokejin521/article/details/78960597)

[springboot构建maven多模块工程](http://blog.csdn.net/zhaokejin521/article/details/79182857)