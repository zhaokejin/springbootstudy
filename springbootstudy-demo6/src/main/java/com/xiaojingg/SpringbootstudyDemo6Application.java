package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo6Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo6Application.class, args);
	}
}
