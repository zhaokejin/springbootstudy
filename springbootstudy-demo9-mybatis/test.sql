/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-12-06 20:41:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `age` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', '20', 'BBB');
INSERT INTO `user` VALUES ('3', '30', 'CCC');
INSERT INTO `user` VALUES ('4', '40', 'DDD');
INSERT INTO `user` VALUES ('5', '50', 'EEE');
INSERT INTO `user` VALUES ('6', '60', 'FFF');
INSERT INTO `user` VALUES ('7', '70', 'GGG');
INSERT INTO `user` VALUES ('8', '80', 'HHH');
INSERT INTO `user` VALUES ('9', '90', 'III');
INSERT INTO `user` VALUES ('10', '100', 'JJJ');
INSERT INTO `user` VALUES ('15', '20', '1231231');
INSERT INTO `user` VALUES ('16', '20', 'AAA');
