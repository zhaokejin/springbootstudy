package com.xiaojingg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstudyDemo9MybatisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootstudyDemo9MybatisApplication.class, args);
	}
}
